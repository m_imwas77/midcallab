﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Model
{
    public class BackEndDbContext : DbContext
    {
        public BackEndDbContext()
        {

        }

        protected override void OnConfiguring
            (DbContextOptionsBuilder optionsBuilder)
        {
            var connectionstrings = new SqlConnectionStringBuilder()
            {
                DataSource = @"DESKTOP-G03KGVS\DBV",
                InitialCatalog = "MidcalLab",
                IntegratedSecurity = true
            };

            optionsBuilder.UseSqlServer(connectionstrings.ToString());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Student>()
            //        .HasOne<Major>(s => s.Major)
            //        .WithMany(g => g.Students)
            //        .HasForeignKey(s => s.MajorId);
            //modelBuilder.Entity<Result>()
            //            .HasOne<Patient>(s => s.Patient)
            modelBuilder.Entity<PatientDispatcher>().HasKey(sc => new { sc.PatientId, sc.DispatcherId});

        }

        //public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<Dispatcher> Dispatchers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<MainTest> MainTests { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientDispatcher> PatientDispatchers{ get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<Sample> Samples { get; set; }
        public DbSet<SampleType> SampleTypes { get; set; }
        public DbSet<Test>  Tests{ get; set; }
    }
}