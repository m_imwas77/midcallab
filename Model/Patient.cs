﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Patient
    {
        public Patient()
        {

        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }
        public int Age { get; set; }
        public int DispatcherId { get; set; }
        public ICollection<Bill> Bills { get; set; }
        public ICollection<PatientDispatcher>  PatientDispatchers { get; set; }
    }
}
