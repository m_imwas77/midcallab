﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Bill
    {
        public Bill()
        {

        }
        public int Id { get; set; }
        public int Paid { get; set; }
        public int Discount { get; set; }

        public int PatientId { get; set; }
        public Patient  Patient{ get; set; }


        public int TestId { get; set; }
        public Test Test{ get; set; }

        
    }
}
