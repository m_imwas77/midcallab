﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Sample
    {
        public Sample()
        {

        }
        public int Id { get; set; }

        public int PatientId { get; set; }
        public Patient Patient{ get; set; }

        public int SampleTypeId { get; set; }
        public SampleType SampleType{ get; set; }

    }
}
