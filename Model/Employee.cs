﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Employee
    {
        public Employee()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Enums.Gender Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public ICollection<Result> Results;
    }
}
