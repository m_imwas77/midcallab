﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class PatientDispatcher
    {
        public PatientDispatcher()
        {

        }
        public int Id { get; set; }

        public int DispatcherId { get; set; }
        public Dispatcher Dispatcher{ get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

    }
}
