﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Test
    {
        public Test()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

        public int MainTestId { get; set; }
        public MainTest MainTest{ get; set; }

        public ICollection<Result> Results;

        public ICollection<Bill> Bills;
    }
}
